import java.util.ArrayList;
import java.util.*;

public class Runner {

	public static void main(String[] args) {

		BillClass I1 = new Internetclass(1, "01/02/2018", "Internet", 60.8, "Lovepreet", 567.8);
		BillClass I2 = new Internetclass(2, "02/03/2017", "Internet", 40.8, "japneet", 450.4);
		BillClass I3 = new Internetclass(3, "23/08/2019", "Internet", 23.9, "simran", 345.9);

		BillClass M1 = new Mobileclass(4, "03/05/2019", "Mobile", 45.9, "Bell", "Free", 987345122, 4.0, 29.0);
		BillClass M2 = new Mobileclass(5, "12/06/2019", "Mobile", 80.6, "Vigan", "Unlimited", 23456422, 5.0, 60.4);
		BillClass M3 = new Mobileclass(6, "13/07/2019", "Mobile", 46.4, "BestBuy", "Limited", 23456743, 56.4, 29.9);

		BillClass H1 = new Hydroclass(7, "14/12/2019", "Hydro", 120.3, "Electric", 45.9);
		BillClass H2 = new Hydroclass(8, "13/02/2019", "Hydro", 134.9, "Interlinx", 34.9);
		BillClass H3 = new Hydroclass(9, "14/12/2019", "Hydro", 23.5, "Water", 55.9);

		Customer c1 = new Customer(01, "Lovepreet", "kaur", "Lovepreet Kaur", "lovepreet23@gmail.com", 56.8);
		Customer c2 = new Customer(02, "Jenelle", "Chen", "Jenelle chen", "Jenellechen2@gmail.com", 34.7);
		Customer c3 = new Customer(03, "harpal", "pall", "harpalpall", "harpalpall1@gmail.com", 345.3);

		c3.addBillClass(I1);

		c1.addBillClass(H1);
		c1.addBillClass(M2);
		c1.addBillClass(I3);

		c2.addBillClass(H3);
		c2.addBillClass(M1);

		CustomerArray customerarray1 = new CustomerArray();
		customerarray1.addCustomer(c1);
		customerarray1.addCustomer(c2);
		customerarray1.addCustomer(c3);
		
		customerarray1.display();

	}

}
