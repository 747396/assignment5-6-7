

public class Hydroclass extends BillClass {
	String AgencyName;
	double Unitsconsumed;
	
	public Hydroclass(int billId,String billDate,String billType,double TotalbillAmount,String AgencyName,double Unitsconsumed) {
		super(billId,billDate,billType,TotalbillAmount);
		this.AgencyName = AgencyName;
		this.Unitsconsumed = Unitsconsumed;
	}

	public String getAgencyName() {
		return AgencyName;
	}

	public void setAgencyName(String agencyName) {
		AgencyName = agencyName;
	}

	public double getUnitsconsumed() {
		return Unitsconsumed;
	}

	public void setUnitsconsumed(double unitsconsumed) {
		Unitsconsumed = unitsconsumed;
	}
	
	@Override
	public void display() {
		super.display();
		System.out.println("Name of agency is " + AgencyName);
		System.out.println("The units consumed is "+ Unitsconsumed);

	}

}
