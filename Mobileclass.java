

public class Mobileclass extends BillClass{
	String MobileManufacturerName;
	String PlanName;
	int mobilenumber;
	double InternetGBused;
	double Minuteused;

	public Mobileclass(int billId,String billDate,String billType,double TotalbillAmount,String MobileManufacturerName,String PlanName,int mobilenumber,double InternetGBused,double Minuteused) {
		
		super(billId,billDate,billType,TotalbillAmount);
		
		this.MobileManufacturerName = MobileManufacturerName;
		this.PlanName = PlanName;
		this.mobilenumber = mobilenumber;
		this.InternetGBused = InternetGBused;
		this.Minuteused = Minuteused;
		
		
		
	}

	public String getMobileManufacturerName() {
		return MobileManufacturerName;
	}

	public void setMobileManufacturerName(String mobileManufacturerName) {
		MobileManufacturerName = mobileManufacturerName;
	}

	public String getPlanName() {
		return PlanName;
	}

	public void setPlanName(String planName) {
		PlanName = planName;
	}

	public int getMobilenumber() {
		return mobilenumber;
	}

	public void setMobilenumber(int mobilenumber) {
		this.mobilenumber = mobilenumber;
	}

	public double getInternetGBused() {
		return InternetGBused;
	}

	public void setInternetGBused(double internetGBused) {
		InternetGBused = internetGBused;
	}

	public double getMinuteused() {
		return Minuteused;
	}

	public void setMinuteused(double minuteused) {
		Minuteused = minuteused;
	}
	
	@Override
	public void display() {
		super.display();
		System.out.println("Mobile Manufacturer name is "+ MobileManufacturerName);
		System.out.println("The Plan is "+ PlanName);
        System.out.println("Mobile Number is "+ mobilenumber);
        System.out.println("Internet is used "+InternetGBused);
        System.out.println("Minutes are "+ Minuteused);
        
	}
	
}
