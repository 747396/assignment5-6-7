import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class Customer implements IDisplay, Comparable<Customer>  {
	
	int CustomerId;
	String firstname;
	String lastname;
	String fullname;
	String emailId;
	double totalamount;
	
	ArrayList<BillClass>billlist = new ArrayList<BillClass>();
	public void addBillClass(BillClass bill) {
		billlist.add(bill);
		
	}
	//public void sortBillClass() {
	//	Collections.sort(this.billlist);
	//}


	//constructor
	
	public Customer(int CustomerId,String firstname,String lastname,String fullname,String emailId,double totalamount) {
		this.CustomerId =CustomerId;
		this.firstname = firstname;
		this.lastname = lastname;
		this.fullname= fullname;
		this.emailId  = emailId;
		this.totalamount = totalamount;
		
		
		
	}
	public ArrayList<BillClass> getbilllist(){
		return billlist;
		
	}
	public void setbilllist(ArrayList<BillClass>BILLLIST) {
		billlist = BILLLIST;
	}
   //Getter and Setters
	
public int getCustomerId() {
	return CustomerId;
}
public void setCustomerId(int customerId) {
	CustomerId = customerId;
}
public String getFirstname() {
	return firstname;
}
public void setFirstname(String firstname) {
	this.firstname = firstname;
}
public String getLastname() {
	return lastname;
}
public void setLastname(String lastname) {
	this.lastname = lastname;
}
public String getFullname() {
	return fullname;
}
public void setFullname(String fullname) {
	this.fullname = fullname;
}
public String getEmailId() {
	return emailId;
}
public void setEmailId(String emailId) {
	this.emailId = emailId;
}
public double getTotalamount() {
	return totalamount;
}
public void setTotalamount(double totalamount) {
	this.totalamount = totalamount;
}

public ArrayList<BillClass> getBilllist() {
	return billlist;
}
public void setBilllist(ArrayList<BillClass> billlist) {
	this.billlist = billlist;
}
public double totalamount() {
	double total = 0;
	for(int i=0;i<billlist.size();i++)
	{
		BillClass B1 = billlist.get(i);
	total+= B1.getTotalbillAmount();	
	}
	return totalamount;
}







	
@Override
public void display() {
	System.out.println("Customer Id :" + CustomerId);
	System.out.println("First Name :" + firstname);
	System.out.println("Last Name :" + lastname);
	System.out.println("Full Name :" + fullname);
	System.out.println("Email Id is :" + emailId);
	
	System.out.println("--------------------------------------------------");
	for(int j=0;j<billlist.size();j++){
	BillClass B1 = billlist.get(j);
	B1.display();
	
	System.out.println("****************************************************");
	}
	
	
	
	}
@Override
public int compareTo(Customer cust) {
	// TODO Auto-generated method stub
	return 0;//
}

}
