

public class Internetclass extends BillClass {
	String ProvideName;
	double InternetGB;
	public Internetclass(int billId,String billDate,String billType,double TotalbillAmount,String ProvideName, double InternetGB) {
		super(billId,billDate,billType,TotalbillAmount);
		this.ProvideName =ProvideName;
		this.InternetGB = InternetGB;
	}
	public String getProvideName() {
		return ProvideName;
	}
	public void setProvideName(String provideName) {
		ProvideName = provideName;
	}
	public double getInternetGB() {
		return InternetGB;
	}
	public void setInternetGB(double internetGB) {
		InternetGB = internetGB;
	}
	
	@Override
	public void display() {
		super.display();
		System.out.println("Provide Name is " + ProvideName);
		System.out.println("Internet GB used " + InternetGB);
        
	}

}
