

public class BillClass implements IDisplay {
	int billId;
	String billDate;
	String billType;
	double TotalbillAmount;
	

	public BillClass(int billId, String billDate, String billType, double TotalbillAmount) {
		this.billId = billId;
		this.billDate = billDate;
		this.billType = billType;
		this.TotalbillAmount = TotalbillAmount;

	}

	public int getBillId() {
		return billId;
	}

	public void setBillId(int billId) {
		this.billId = billId;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public String getBillType() {
		return billType;
	}

	public void setBillType(String billType) {
		this.billType = billType;
	}

	public double getTotalbillAmount() {
		return TotalbillAmount;
	}

	public void setTotalbillAmount(double totalbillAmount) {
		TotalbillAmount = totalbillAmount;
	}

	@Override
	public void display() {
		System.out.println("Bill id is " +billId);
		System.out.println("BillDate is " +billDate);
		System.out.println("Bill Type is " +billType);
		System.out.println("TotalbillAmount is " +TotalbillAmount);
		
		

	}
}
